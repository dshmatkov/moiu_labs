import sympy
import numpy as np


with open('test4.in', 'r') as file_:
    n = int(file_.readline())
    x = [sympy.Symbol('x{}'.format(i), real=True) for i in xrange(n)]

    f = eval(file_.readline())
    m = int(file_.readline())

    g = [eval(file_.readline()) for _ in xrange(m)]
    lam = [sympy.Symbol('lam{}'.format(i)) for i in xrange(m)]
    x_val = map(float, file_.readline().split())

# indexes = [i for i in xrange(m) if g[i].subs(dict(zip(x, x_val))) == 0.0]
# indexes = []
# if not indexes:
#     indexes = range(m)

dfdx = [sympy.diff(f, x[i]).subs(dict(zip(x, x_val))) for i in xrange(n)]
dgdx = [
    [sympy.diff(g[i], x[j]).subs(dict(zip(x, x_val))) for i in xrange(m)]
    for j in xrange(n)
]

# system = []
# for i in xrange(n):
#     func = dfdx[i]
#     for j in indexes:
#         func += lam[j] * dgdx[i][j]
#     system.append(func)
system = [dfdx[i] + sum(map(lambda a, b: a * b, lam, dgdx[i])) for i in xrange(n)]
lams = sympy.solve(system)

ok = True
if isinstance(lams, dict):
    try:
        ok = all(val >= 0 for (var, val) in lams.iteritems() if str(var).startswith('lam'))
    except:
        ok = True
    lams = [lams]

if not lams or not ok:
    print 'no solution for system'
    print 'not optimal'
    exit()

dgdx2 = zip(*dgdx)

L = f + sum(map(lambda a, b: a*b, lam, g))
for solution in lams:
    if x_val:
        xx = zip(x, x_val)
    else:
        xx = filter(lambda (var, val): str(var).startswith('x'), solution.iteritems())
    l = [sympy.Symbol('l{}'.format(i)) for i in xrange(n)]
    l_system = [
        sum(map(lambda a, b: a*b, dgdx2[i], l))
        for i in xrange(m) if g[i].subs(dict(xx)) == 0.0
    ]
    l = sympy.solve(l_system)
    if isinstance(l, dict):
        l = [l]
    new_l = []
    for temp_l in l:
        new_l.append(np.matrix([temp_l[sympy.Symbol('l{}'.format(i))] for i in xrange(n)]))

    if any(g[i].subs(dict(xx)) > 0 for i in xrange(m)):
        print 'not solution', xx
        continue
    # if any(g[i].subs(dict(xx)) != 0.0 for i in xrange(m)):
    #     print 'not optimally', xx
    #     continue
    H = [
        [float(sympy.diff(L, i, j).subs(dict(zip(x, x_val) + solution.items()))) for j in x]
        for i in x
    ]
    H = np.matrix(H)

    l = [(l * H * l.T) for l in new_l]
    positive = any(val > 0.0 for val in l)
    negative = any(val < 0.0 for val in l)
    print new_l
    print l
    if positive and negative:
        print 'not optimal',
    elif positive:
        print 'local minimum',
    elif negative:
        print 'local maximum',
    else:
        print 'local extremum',
    print xx

    # if all(np.linalg.det(H[0:i, 0:i]) > 0.0 for i in xrange(1, n+1)):
    #     print 'local minimum',
    # elif all(np.linalg.det(H[0:i, 0:i]) < 0.0 for i in xrange(1, n+1)):
    #     print 'local maximum',
    # elif all(np.linalg.det(H[0:i, 0:i]) >= 0.0 for i in xrange(1, n+1))\
    #         or all(np.linalg.det(H[0:i, 0:i]) <= 0.0 for i in xrange(1, n+1)):
    #     print 'saddle point',
    # else:
    #     print 'not optimal',
    # print xx
