import sys

import numpy as np

EPS = 0.00000001


def read(filein):
    def get_vector(f):
        v = map(float, f.readline().split())
        v = np.matrix(v).T
        return v

    def get_matrix(f, size):
        arr = [map(float, f.readline().split()) for _ in xrange(size)]
        arr = np.matrix(arr)
        return arr

    with open(filein, 'r') as f:
        n, m = map(int, f.readline().split())
        A = get_matrix(f, 3)
        b = get_vector(f)
        line = f.readline()
        c = None
        D = None
        B = None
        d = None
        while line[0] != 'x':
            if line[0] == 'c':
                c = get_vector(f)
                # c *= 0
            elif line[0] == 'D':
                D = get_matrix(f, n)
                # D = np.identity(n)
                # D *= 100
            elif line[0] == 'B':
                B = get_matrix(f, 3)
            elif line[0] == 'd':
                d = get_vector(f)
            line = f.readline()
        if c is None:
            c = (-d.T * B).T
        if D is None:
            D = B.T * B
        x = map(float, f.readline().split())
        x = np.matrix(x).T
        J_op = map(int, f.readline().split())
        J_star = map(int, f.readline().split())
    return n, m, A, b, c, D, x, J_op, J_star


n, m, A, b, c, D, x, J_op, J_star = read('6.in')

# print A
# print D
# print c
# print c.T * x + 0.5 * x.T * D * x

print (c.T * x + x.T * D * x / 2)[0, 0]

J = range(n)

next_step = 1
cycle = 1

while True:

    # calculate deltas
    if next_step == 1:
        c_new = D * x + c
        A_op = np.matrix([[A[i, j] for j in J_op] for i in xrange(m)])
        # print np.linalg.inv(A_op)
        c_new_op = c_new.take(list(J_op))
        u = -c_new_op * A_op.I
        delta = u * A + c_new.T

        Jn = set(J) - set(J_star)
        if all(delta[0, j] >= -EPS for j in Jn):
            print 'ok'
            break
        for j0 in Jn:
            if delta[0, j0] < -EPS:
                break
    # build 'new direction'
    # print x.T
    l = [0] * n
    l[j0] = 1
    D_star = [[D[i, j] for j in J_star] for i in J_star]
    A_star = [[A[i, j] for j in J_star] for i in xrange(m)]
    Djj = [D[i, j0] for i in J_star]
    Aj0 = [A[i, j0] for i in xrange(m)]
    size = len(D_star) + m
    H = [[0] * size for _ in xrange(size)]
    for i in xrange(size - m):
        for j in xrange(size - m):
            H[i][j] = D_star[i][j]
    for i in xrange(m):
        for j in xrange(len(A_star[0])):
            H[size - m + i][j] = A_star[i][j]
            H[j][size - m + i] = A_star[i][j]
    H = np.matrix(H)
    bb = []
    for i in Djj:
        bb.append([i])
    for i in Aj0:
        bb.append([i])
    bb = np.matrix(bb)
    try:
        vector = -H.I * bb
    except np.linalg.LinAlgError:
        print 'no solution'
        sys.exit()
    for i, j in enumerate(J_star):
        l[j] = vector[i, 0]

    # calculate steps-thetas
    theta = [float('inf')] * n
    for j in J_star:
        if l[j] >= -EPS:
            theta[j] = float('inf')
        else:
            theta[j] = -x[j, 0] / l[j]

    l = np.matrix(l)
    sdelta = (l * D * l.T)[0, 0]
    # print theta
    theta[j0] = float('inf') if sdelta == 0.0 else abs(delta[0, j0]) / sdelta
    theta0 = min(theta)
    if theta0 == float('inf'):
        print 'is infinity'
        # print 'no solution'
        sys.exit()
    j_star = theta.index(theta0)
    x += theta0 * l.T
    if theta0 == float('inf'):
        print 'is infinity'

    # a
    if j_star == j0:
        J_star.append(j0)
        next_step = 1
    # b
    elif j_star in (set(J_star) - set(J_op)):
        J_star.remove(j_star)
        delta[0, j0] += theta0 * sdelta
        next_step = 3
    # c, d
    else:
        s = J_op.index(j_star)
        # print J_star
        ok = False
        e = np.matrix(np.identity(len(A_op)))
        for j_plus in (set(J_star) - set(J_op)):
            # print J_op, J_star
            # print j_star
            if (e[s, :] * A_op.I * A[:, j_plus])[0, 0] != 0.0:
                ok = True
                break
        if ok:
            J_op.remove(j_star)
            J_op.append(j_plus)
            J_star.remove(j_star)
            delta[0, j0] += theta0 * sdelta
            next_step = 3
        else:
            J_op.remove(j_star)
            J_op.append(j0)
            J_star.remove(j_star)
            J_star.append(j0)
            next_step = 1
    J_op.sort()
    J_star.sort()
    cycle += 1




res = []
for i in xrange(3):
    res.append((A * x)[i, 0])
print res

res = []
for i in xrange(3):
    res.append(b[i, 0])
print res

print (c.T * x + x.T * D * x / 2)[0, 0]