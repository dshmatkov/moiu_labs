import numpy as np


def get_data():
    with open('input.txt') as f:
        n = int(f.readline())
        c = map(float, f.readline().split())
        m = int(f.readline())
        A = [map(float, f.readline().split()) for _ in xrange(m)]
        b = map(float, f.readline().split())
        x = map(float, f.readline().split())
    return n, m, A, b, c, x


def solution(n, m, A, c, x):
    Jb = set(i for i in xrange(n) if x[i] != 0)
    Jn = set(i for i in xrange(n) if x[i] == 0)

    A_matrix = np.matrix(A)

    cnt = 1

    while True:
        B = np.matrix([[A[i][j] for j in Jb] for i in xrange(m)]).I

        ##########
        # step 1 #
        ##########
        u = np.matrix([c[j] for j in Jb]) * B

        print u
        print ""

        delta = [0] * n
        for j in Jn:
            delta[j] = (u * A_matrix.T[j].T).item(0, 0) - c[j]

        ##########
        # step 2 #
        ##########
        if all(delta[j] >= 0 for j in Jn):
            return x

        ##########
        # step 3 #
        ##########
        j0 = delta.index(min(delta))
        z = B * A_matrix.T[j0].T
        if all(z.item(i, 0) <= 0 for i in xrange(m)):
            return None

        ##########
        # step 4 #
        ##########
        theta0, s, js = min(
            (x[ji] / z.item(i, 0), i, ji) for i, ji in enumerate(Jb) if
            z.item(i, 0) > 0)

        ##########
        # step 5 #
        ##########
        for j in Jn:
            if j != j0:
                x[j] = 0
        x[j0] = theta0
        for i, ji in enumerate(Jb):
            x[ji] -= theta0 * z.item(i, 0)

        Jb.remove(js)
        Jn.remove(j0)
        Jb.add(j0)
        Jn.add(js)

        cnt += 1


if __name__ == '__main__':
    n, m, A, b, c, x = get_data()
    result = solution(n, m, A, c, x)
    if result:
        print result
    else:
        print 'No solution'