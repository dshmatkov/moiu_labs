#include <iostream>
#include <cstdio>
#include <cmath>

#define N 100

using namespace std;

int n, indexes[N], column[N];
float  c[N][N], b[N][N], a[N][N];

void calculate_next_b(int j){
    float vect[N], d[N][N], bi[N][N];
    for (int i = 0; i < n; ++i){
        for (int k = 0; k < n; ++k){
            d[i][k] = 0;
            bi[i][k] = 0;
        }
        d[i][i] = 1;
        vect[i] = c[i][column[j]];
    }
    float z[N];
    for (int i = 0; i < n; ++i){
        z[i] = 0;
    }

    for (int i = 0; i < n; ++i){
        for (int k = 0; k < n; ++k){
            //cout << z[i] << endl;
            z[i] += b[i][k] * vect[k];
            //cout << z[i] << b[i][k] << vect[k]<< endl;
        }
    }

    float zj = z[j];
    z[j] = -1;
    zj = -1 / zj;
    //cout << "zj " << zj << endl;
    for (int i = 0; i < n; ++i){
        z[i] *= zj;
        d[i][j] = z[i];
    }

    /*
    cout << "b\n";
    for (int q = 0; q < n; ++q){
        for (int l = 0; l < n; ++l){
            cout << b[q][l] << " ";
        }
        cout << endl;
    }
    cout << "endb\n";

    cout << "d\n";
    for (int q = 0; q < n; ++q){
        for (int l = 0; l < n; ++l){
            cout << d[q][l] << " ";
        }
        cout << endl;
    }
    cout << "endd\n";

    cout << "zz " << z[0] << " " << z[1]<< " " << z[2] << endl;;
    */

    for (int i = 0; i < n; ++i){
        for (int j = 0; j < n; ++j){
            for (int k = 0; k < n; ++k){
                bi[i][j] += d[i][k] * b[k][j];
            }
        }
    }



    for (int i = 0; i < n; ++i){
        for (int j = 0; j < n; ++j){
            b[i][j] = bi[i][j];
        }
    }
}

float find_aj(int i, int j){
    float result = 0;
    for (int k = 0; k < n; ++k){
        result += b[i][k] * c[k][j];
    }
    return result;
}

int main() {

    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    cin >> n;

    for (int i = 0; i < n; ++i){
        for (int j = 0; j < n; ++j){
            cin >> c[i][j];
        }
        b[i][i] = 1;
        a[i][i] = 1;
    }


    for (int j = 0; j < n; ++j){
        column[j] = -1;

        //cout << "Iteration " << j << endl;

        for (int i = 0; i < n; ++i){
            if (indexes[i] == 0){
                float aj = find_aj(j, i);
                //cout << i << " " << aj << endl;
                if (abs(aj) > 1e-7){
                    indexes[i] = 1;
                    column[j] = i;
                    break;
                }
            }
        }

        if (column[j] == -1){
            cout << "Can't be found.";
            return 0;
        }
        for (int i = 0; i < n; ++i){
            a[i][j] = c[i][column[j]];
        }
        calculate_next_b(j);
    }

    for (int i = 0; i < n; ++i){
        for (int j = 0; j < n; ++j){
            cout << b[column[i]][j] << " ";
        }
        cout << endl;
    }

    return 0;
}
