import copy

INPUT = 'input.txt'
OUTPUT = 'output.txt'
INF = 1000111000


def get_data():
    with open(INPUT, 'r') as f:
        n, m = [int(str) for str in f.readline().split(' ')][:2]

        store = [int(str) for str in f.readline().split(' ')][:n]
        requirement = [int(str) for str in f.readline().split(' ')][:m]

        transport = []

        for i in xrange(n):
            transport.append(
                [int(str) for str in f.readline().split(' ')][:m]
            )

    return n, m, store, requirement, transport


def find_flow(store, req, m):
    store_summ = 0
    for item in store:
        store_summ += item

    req_summ = 0
    for item in req:
        req_summ += item

    n = len(store)

    row_summ = [0 for i in xrange(n)]
    col_summ = [0 for i in xrange(m)]

    transport = [
        [0 for j in xrange(m)]
        for i in xrange(n)
    ]

    i = 0
    j = 0
    while (i < n) and (j < m):
        add_val = min(store[i] - row_summ[i], req[j] - col_summ[j])
        row_summ[i] += add_val
        col_summ[j] += add_val
        transport[i][j] = add_val
        if store[i] - row_summ[i] > 0:
            j += 1
        else:
            i += 1
    return transport


def is_cycle(cells, n, m, start_point=None):
    xx = [set() for _ in xrange(n)]
    yy = [set() for _ in xrange(m)]

    xex = [set() for _ in xrange(n)]
    yex = [set() for _ in xrange(m)]

    for cell in cells:
        xx[cell[0]].add(cell)
        yy[cell[1]].add(cell)

    def find_cycle(point, direct, cycle):
        cycle = copy.deepcopy(cycle)

        cycle.append(point)

        xex[point[0]].add(point)
        yex[point[1]].add(point)

        xx[point[0]].discard(point)
        yy[point[1]].discard(point)

        if direct:
            if len(xex[point[0]]) > 1:
                return True, cycle
            possible_next_elements = xx[point[0]]
        else:
            if len(yex[point[1]]) > 1:
                return True, cycle
            possible_next_elements = yy[point[1]]

        for item in possible_next_elements:
            result, found_cycle = find_cycle(item, 1 - direct, cycle)
            if result:
                return True, found_cycle

        xx[point[0]].add(point)
        yy[point[1]].add(point)

        xex[point[0]].discard(point)
        yex[point[1]].discard(point)

        return False, []

    if start_point:
        cell = start_point

        result, cycle = find_cycle(cell, 1, [])
        if result:
            return result, cycle

        result, cycle = find_cycle(cell, 0, [])
        if result:
            return result, cycle

    else:
        for cell in cells:
            result, cycle = find_cycle(cell, 1, [])
            if result:
                return True, cycle

            result, cycle = find_cycle(cell, 0, [])
            if result:
                return True, cycle

    return False, []


def find_basis(n, m, flow):
    check_n = n + m - 1
    not_null_count = 0

    basis = []

    for i in xrange(n):
        for j in xrange(m):
            if flow[i][j]:
                not_null_count += 1
                basis.append(
                    (i, j,)
                )

    basis_set = set(basis)

    for _ in xrange(check_n - not_null_count):
        for i in xrange(n):
            for j in xrange(m):
                if (
                        (i, j) not in basis_set
                        and not is_cycle(basis + [(i, j)], n, m)[0]
                        and check_n - not_null_count > 0
                ):
                    basis.append(
                        (i, j,)
                    )
                    basis_set = set(basis)

    return basis


def pretty_print(matrix):
    for str in matrix:
        print (len(str)*'{}\t').format(*str)

if __name__ == '__main__':
    n, m, store, requirement, transport = get_data()

    store_summ = 0
    for item in store:
        store_summ += item

    requirement_summ = 0
    for item in requirement:
        requirement_summ += item

    if store_summ > requirement_summ:
        diff = store_summ - requirement_summ
        requirement.append(diff)
        for row in transport:
            row.append(0)

    m += 1
    for i in xrange(n):
        transport[i].append(0)

    flow = find_flow(store, requirement, m)

    basis = find_basis(n, m, flow)

    iteration = 0

    # for string in flow:  # delete this
    #     print string  # delete this

    while True:
        # print ''  # delete this
        # print ''  # delete this

        iteration += 1
        print 'Iteration: {}'.format(iteration)  # delete this

        u = [-INF for _ in xrange(n)]
        v = [-INF for _ in xrange(m)]
        v[m - 1] = 0

        work_matrix = [
            [-INF for _ in xrange(m)]
            for _ in xrange(n)
        ]

        for x, y in basis:
            work_matrix[x][y] = transport[x][y]

        for _ in xrange(len(basis)):
            for x, y in basis:
                if u[x] == -INF and v[y] != -INF:
                    u[x] = transport[x][y] - v[y]
                if u[x] != -INF and v[y] == -INF:
                    v[y] = transport[x][y] - u[x]

        # print u  # delete this
        # print v  # delete this

        pos = None
        for i in xrange(n):
            for j in xrange(m):
                if (i, j) not in basis:
                    if not pos:
                        pos = (i, j)
                    work_matrix[i][j] = transport[i][j] - u[i] - v[j]
                    if work_matrix[pos[0]][pos[1]] > work_matrix[i][j]:
                        pos = (i, j)

        # for string in work_matrix:  # delete this
        #     print string            # delete this

        if work_matrix[pos[0]][pos[1]] >= 0:
            break

        basis = [pos] + basis
        _, cycle = is_cycle(basis, n, m, pos)

        # print basis  # delete this

        minus_cycle = cycle[1::2]
        plus_cycle = cycle[0::2]

        # print minus_cycle  # delete this
        # print plus_cycle  # delete this

        pos = minus_cycle[0]
        for item in minus_cycle:
            if flow[item[0]][item[1]] < flow[pos[0]][pos[1]]:
                pos = item

        # print pos  # delete this

        teta = flow[pos[0]][pos[1]]
        # print teta  # delete this

        for item in minus_cycle:
            flow[item[0]][item[1]] -= teta

        for item in plus_cycle:
            flow[item[0]][item[1]] += teta

        basis = set(basis)
        basis.discard(pos)

        basis = list(basis)
        # print basis  # delete this

        # print ''  # delete this
        # pretty_print(flow)  # delete this

    pretty_print(flow)
    summ = 0

    for i in xrange(n):
        for j in xrange(m):
            summ += transport[i][j] * flow[i][j]

    print summ




