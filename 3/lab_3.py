import numpy

f_in = 'input.txt'
f_out = 'output.txt'


def get_data():
    with open(f_in, 'r') as f:

        # read count of variables
        n = int(f.readline())

        # read function coeffs
        func = [
            int(c) for c in f.readline().split(' ')
        ]

        # read count of limitations
        m = int(f.readline())

        # read limitation equations
        equations = []
        for i in xrange(m):
            equation = [
                float(c) for c in f.readline().split(' ')
            ]
            equations.append(equation)

        # read answer on equations
        b = [
            [int(c)] for c in f.readline().split(' ')
        ]

        # read starting plan
        x = [
            float(c) for c in f.readline().split(' ')
        ]

        func = numpy.matrix([func])

        equations = numpy.matrix(equations)

        b = numpy.matrix(b)

        x = numpy.matrix([x])

    return n, func, m, equations, b, x


def get_columns(matrix, columns):
    new_matrix = []
    for row in matrix:
        new_row = []
        for column in columns:
            new_row.append(row.item((0, column)))
        new_matrix.append(new_row)
    return numpy.matrix(new_matrix)


def preprocess(n, f, x, a):
    all_j = set(xrange(n))
    j_b = set()

    for i in xrange(n):
        if x.item((0, i)) != 0:
            j_b.add(i)

    j_n = all_j - j_b

    a_b = get_columns(a, j_b)
    a_n = get_columns(a, j_n)

    # a_i is B, but it is a_b_inverted so it is a_i
    a_i = a_b.I

    f_b = get_columns(f, j_b)
    f_n = get_columns(f, j_n)

    k_n = numpy.subtract(
        numpy.dot(
            numpy.dot(f_b, a_i),
            a_n
        ),
        f_n
    )

    return all_j, j_b, j_n, a_i, k_n


def find_jk(xi, j_b):
    result = None
    i = 0
    while (result is None) and (i < len(j_b)):
        if xi[i, 0] < 0:
            result = i
        i += 1
    return result, (j_b[result] if result is not None else None)


def find_sigma0(k_n, mu_j):
    n = k_n.shape[1]

    result = None
    minim = 1e+100

    for i in xrange(n):
        if mu_j[0, i] < 0:
            temp = - k_n[0, i] / mu_j[0, i]
            if temp < minim:
                result = i
                minim = temp

    return result


def solve():
    n, func, m, a, b, x = get_data()
    all_j, j_b, j_n, a_i, k_n = preprocess(n, func, x, a)

    is_unsolved = False
    is_solved = False

    iteration = 0

    while not (is_solved or is_unsolved):

        iteration += 1
        print iteration

        j_b = list(j_b)
        j_b.sort()
        j_n = list(all_j - set(j_b))
        j_n.sort()

        ##########
        # step 1 #
        ##########
        xi_b = numpy.dot(a_i, b)
        print 'xi_b', xi_b

        ################
        # step 2 and 3 #
        ################
        k, j_k = find_jk(xi_b, j_b)

        if k is None:
            is_solved = True
            break

        mu_j = numpy.dot(
            a_i[k, :],
            get_columns(a, j_n)
        )

        print 'Ai k string', a_i[k, :]
        print 'mu j', mu_j

        ######################
        # step 4 and 5 and 6 #
        ######################
        j = find_sigma0(k_n, mu_j)

        if j is None:
            is_unsolved = True
            break

        sigma0 = - k_n[0, j] / mu_j[0, j]

        j0 = j_n[j]

        ##########
        # step 7 #
        ##########

        j_b = set(j_b)
        j_b -= {j_k}
        j_b |= {j0}
        j_b = list(j_b)
        j_b.sort()

        j_n = set(j_n)
        j_n -= {j0}
        j_n |= {j_k}
        j_n = list(j_n)
        j_n.sort()

        ##########
        # step 8 #
        ##########

        new_k_n = []
        for i in xrange(len(j_n)):
            if j_n[i] == j_k:
                new_k_n.append(sigma0)
            else:
                new_k_n.append(k_n[0, i] + sigma0 * mu_j[0, i])

        k_n = numpy.matrix([new_k_n])

        ##########
        # step 9 #
        ##########

        a_i = get_columns(a, j_b).I

    x = [0 for j in all_j]
    j_b = list(j_b)
    j_b.sort()
    for i, j in enumerate(j_b):
        x[j] = xi_b[i, 0]

    res = []
    for i in xrange(m):
        res.append(0)
        for j in xrange(n):
            res += a[i, j] * x[j]

    if is_unsolved:
        return None
    else:
        return (x, res)

if __name__ == '__main__':
    result = solve()
    with open(f_out, 'w') as f:
        if result is None:
            f.write('No solutions')
        else:
            for i in result:
                print i


