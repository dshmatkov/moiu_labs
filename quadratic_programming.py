import numpy as np
from numpy.linalg import inv as m_inv

EPS = 1e-8


def get_ast_matrix(matrix, J_ast):
    return matrix[J_ast, :][:, J_ast]


def compute_function(c, D, x):
    return np.dot(x, c) + 0.5*np.dot(np.dot(x, D), x)


def solve_square_problem(x, J_b, J_ast, D, c, A):
    """
    :param x: x
    :param J_b: basis plan
    :param J_ast: right plan
    :param D: D
    :param c: c
    :param A: A
    :return: x, J_b, J_ast
    """
    iterations = 0
    not_full_iterations = 0
    function_value = compute_function(c, D, x)
    m, n = A.shape
    print 'Func: ', function_value
    J_res = list(set(J_ast)-set(J_b))  # J_ast\J_b
    J_n = list(set(range(n))-set(J_ast))  # J\J_ast
    while True:
        A_base = A[:, J_b]
        A_base_inv = m_inv(A_base)
        if A_base_inv is None:
            print 'Can not invert A_base'
            return None, None, None
        c_new = c + np.dot(D, x)
        # potentials
        u = -np.dot(c_new[J_b], A_base_inv)
        # evaluations for j in J_n
        delta = np.dot(u, A[:, J_n]) + c_new[J_n]

        # check the condition delta >= 0 for j in J_n
        if np.all(delta >= -EPS):
            print 'Iterations: ', iterations
            print 'Not full iterations: ', not_full_iterations
            return x, J_b, J_ast
        k = np.argmin(delta)
        j0 = J_n[k]
        go_to_step_3 = True
        # Use this cycle, cause we need to go to the 3-rd step in some cases
        while go_to_step_3:
            # the direction of plan change
            l = np.zeros((n,))
            l[j0] = 1

            # Build H:
            #      (D_ast, A_ast')
            # H =  (A_ast, 0     )
            #     (D_ast_j0)
            # b = (  A_j0  ) , D_ast_j0 = (d_i_j0, i in J_ast)
            #
            # l_ast - first elements of H^(-1)*
            D_ast = get_ast_matrix(D, J_ast)
            A_ast = A[:, J_ast]
            additional_matrix = np.zeros((m, m))

            top_half_matrix = np.hstack((D_ast, A_ast.T))
            bottom_half_matrix = np.hstack((A_ast, additional_matrix))

            H_ast = np.vstack((top_half_matrix, bottom_half_matrix))
            right_equ_part = -np.hstack((D[J_ast, j0], A[:, j0]))

            H_ast_inv = m_inv(H_ast)
            l[J_ast] = np.dot(H_ast_inv, right_equ_part)[:len(J_ast)]

            thetas = []
            # For all l_j, j in J_ast
            for i, lj in enumerate(l[J_ast]):
                j = J_ast[i]
                if lj < -EPS:
                    thetas.append(-x[j]/lj)
                else:
                    thetas.append(np.inf)
            # delta = l'*D*l
            dlt = np.dot(np.dot(l, D), l)
            # use k, cause delta consists ONLY of elements not in plan
            theta_j0 = np.abs(delta[k])/dlt if dlt > EPS else np.inf

            if theta_j0 == np.inf and (np.array(thetas) == np.inf).all():
                print 'Not limited on the plans set.'
                return None, None, None

            # Get min of all thetas
            s = np.argmin(thetas)
            j_ast = J_ast[s]
            theta_0 = np.min(thetas)

            j_ast = j_ast if theta_0 < theta_j0 else j0
            theta_0 = min(theta_0, theta_j0)

            # use chosen theta and computed direction
            x += theta_0*l

            # Check the conditions to proceed computations:
            # 1) j_ast = j_0
            # 2) j_ast in J_res = J_ast\J_b
            # 3) j_ast = j_s in J_b AND exists j_plus in J_res -> Ab_inv[s,:]*A[:,j_plus] != 0
            # 4) j_ast = j_s in J_b AND ab_inv[s, :]*A[:, j] for j in J_res
            #       OR
            #    J_ast = J_b
            print 'Func: ', compute_function(c, D, x)
            iterations += 1
            if j_ast == j0:
                # 1) condition
                # J_b_new - same
                # J_ast_new - + j0
                # goto 1 step
                J_ast.append(j0)
                J_res.append(j0)
                J_n.remove(j0)
                go_to_step_3 = False
            elif j_ast in J_res:
                # 2) condition
                # J_b_new - same
                # J_ast_new - -j_ast
                # j0_new - same
                # delta_j0 += theta_0*dlt
                # goto 3 step
                J_ast.remove(j_ast)
                J_res.remove(j_ast)
                J_n.append(j_ast)
                delta[k] += theta_0*dlt
                not_full_iterations += 1
            elif j_ast in J_b:
                j_plus_tester = np.dot(A_base_inv[s, :], A[:, J_res])
                if np.all(np.abs(j_plus_tester) <= EPS) or J_ast == J_b:
                    # 4) condition
                    # J_b_new - +j0-j_ast
                    # J_ast_new - -//-
                    # goto 1 step
                    J_b.remove(j_ast)
                    J_b.append(j0)
                    J_ast.remove(j_ast)
                    J_ast.append(j0)
                    J_n.append(j_ast)
                    J_n.remove(j0)
                    go_to_step_3 = False
                else:
                    # 3) condition
                    # J_b_new - +j_plus-j_ast
                    # J_ast - -j_ast
                    # j0_new - same
                    # delta_j0 += theta_0*dlt
                    # goto 3 step
                    j_plus = J_res[np.argwhere(np.abs(j_plus_tester) > EPS)[0][0]]
                    J_b.remove(j_ast)
                    J_b.append(j_plus)
                    J_ast.remove(j_ast)
                    J_res.remove(j_plus)
                    J_n.append(j_ast)
                    delta[k] += theta_0*dlt
                    not_full_iterations += 1


if __name__ == '__main__':
    A = np.array([
        [-2, 4, -2, -1, -3, 2, 1, 1, 0],
        [1, -3, 0, 1, 1, 0, -3, 4, 1],
        [1, 1, 1, 1, -4, 0, 4, -3, 1],
    ], np.float64)
    # B = np.array([
    #     [2, -3, 1, 0, 1, 1, 1, -2, 1],
    #     [1, 1, 0, 0, 1, 1, 0, 4, 0],
    #     [0, 0, 1, 3, -1, 2, -3, -5, 0]
    # ], np.float64)
    B = np.array([
        [1, 1, 2, -1, 1, -1, 5, 1, 1],
        [0, 0, 2, 3, 0, 4, -3, 2, 1]
    ], np.float64)
    d = np.array([np.sum(B[0, :]), np.sum(B[1, :])], np.float64)
    print d
    D = np.dot(B.T, B)
    c = -np.dot(d, B)
    x = np.array([0, 0, 0, 0, 0, 0, 0, 0, 2], np.float64)
    J_b = [4, 6, 8]
    J_ast = [4, 6, 8]
    x_opt, J_b, J_ast = solve_square_problem(x, J_b, J_ast, D, c, A)
    if x_opt is not None:
        print 'x optimal: [\n\t' + '\n\t'.join(map(str, x_opt)) + '\n]'
        print 'f(x_optimal) = ', np.dot(c, x_opt) + 0.5*np.dot(np.dot(x_opt, D), x_opt)
        print J_b, J_ast
    # A = np.array([float
    #     [1, 7, -1, 7, -8],
    #     [0, 1, 8, 9, 7],
    #     [0, 0, 1, 1, 1]
    # ])
    # b = np.array([14, 18, 2])
    #
    # B = np.array([
    #     [1, 1, -1, 0, 3, 4, -2, 1],
    #     [2, 6, 0, 0, 1, -5, 0, -1],
    #     [-1, 2, 0, 0, -1, 1, 1, 1]
    # ])
    # d = np.array([7, 3, 3])
    #
    # D = np.array([
    #     [2, 2, 2, 2, 2],
    #     [2, 2, 2, 2, 2],
    #     [2, 2, 2, 2, 2],
    #     [2, 2, 2, 2, 2],
    #     [2, 2, 2, 2, 2]
    # ])
    # c = np.array([7, 0, 0, 9, -9], np.float128)
    #
    # x = np.array([1, 1, 1, 1, 0], dtype=np.float128)
    # J_b = [0, 1, 2]
    # J_ast = [0, 1, 2, 3]
    # x_opt, _, _ = solve_square_problem(x, J_b, J_ast, D, c, A)
    # if x_opt is not None:
    #     print 'x optimal: [\n\t' + '\n\t'.join(map(str, x_opt)) + '\n]'
    #     print 'f(x_optimal) = ', np.dot(c, x_opt) + 0.5*np.dot(np.dot(x_opt, D), x_opt)
    # print '\n\nEx.1\n'
    # A = np.array([
    #     [1, 2, 0, 1, 0, 4, -1, -3],
    #     [1, 3, 0, 0, 1, -1, -1, 2],
    #     [1, 4, 1, 0, 0, 2, -2, 0]
    # ])
    # b = np.array([4, 5, 6])
    #
    # B = np.array([
    #     [1, 1, -1, 0, 3, 4, -2, 1],
    #     [2, 6, 0, 0, 1, -5, 0, -1],
    #     [-1, 2, 0, 0, -1, 1, 1, 1]
    # ])
    # d = np.array([7, 3, 3])
    #
    # D = np.dot(B.T, B)
    # c = -np.dot(d, B)
    #
    # x = np.array([0, 0, 6, 4, 5, 0, 0, 0], dtype=np.float128)
    # J_b = [2, 3, 4]
    # J_ast = [2, 3, 4]
    # x_opt, _, _ = solve_square_problem(x, J_b, J_ast, D, c, A)
    # if x_opt is not None:
    #     print 'x optimal: [\n\t' + '\n\t'.join(map(str, x_opt)) + '\n]'
    #     print 'f(x_optimal) = ', np.dot(c, x_opt) + 0.5*np.dot(np.dot(x_opt, D), x_opt)
    #
    # print '\n\nProblem 1\n'
    # A = np.array([
    #     [11, 0, 0, 1, 0, -4, -1, 1],
    #     [1, 1, 0, 0, 1, -1, -1, 1],
    #     [1, 1, 1, 0, 1, 2, -2, 1]
    # ], np.float128)
    # b = np.array([8, 2, 5], np.float128)
    # d = np.array([6, 10, 9], np.float128)
    #
    # B = np.array([
    #     [1, -1, 0, 3, -1, 5, -2, 1],
    #     [2, 5, 0, 0, -1, 4, 0, 0],
    #     [-1, 3, 0, 5, 4, -1, -2, 1]
    # ], np.float128)
    # D = np.dot(B.T, B)
    # c = -np.dot(d, B)
    # x = np.array([0.7273, 1.2727, 3, 0, 0, 0, 0, 0], np.float128)
    # J_b = [0, 1, 2]
    # J_ast = [0, 1, 2]
    # x_opt, _, _ = solve_square_problem(x, J_b, J_ast, D, c, A)
    # if x_opt is not None:
    #     print 'x optimal: [\n\t' + '\n\t'.join(map(str, x_opt)) + '\n]'
    #     print 'f(x_optimal) = ', np.dot(c, x_opt) + 0.5*np.dot(np.dot(x_opt, D), x_opt)
    #
    # print '\n\nProblem 2\n'
    # A = np.array([
    #     [2,-3,1,1,3,0,1,2],
    #     [-1,3,1,0,1,4,5,-6],
    #     [1,1,-1,0,1,-2,4,8],
    # ], np.float128)
    #
    # B = np.array([
    #     [1,0,0,3,-1,5,0,1],
    #     [2,5,0,0,0,4,0,0],
    #     [-1,9,0,5,2,-1,-1,5]
    # ], np.float128)
    #
    # d = np.array([6, 10, 9], np.float128)
    #
    # c = np.array([-13,-217,0,-117,-27,-71,18,-99], np.float128)
    #
    # D = np.dot(B.T, B)
    # x = np.array([0, 2, 0, 0, 4, 0, 0, 1], np.float128)
    # J_b = [1, 4, 7]
    # J_ast = [1, 4, 7]
    # x_opt, _, _ = solve_square_problem(x, J_b, J_ast, D, c, A)
    #
    # if x_opt is not None:
    #     print 'x optimal: [\n\t' + '\n\t'.join(map(str, x_opt)) + '\n]'
    #     print 'f(x_optimal) = ', np.dot(c, x_opt) + 0.5*np.dot(np.dot(x_opt, D), x_opt)
    #
    # print '\n\nProblem 3\n'
    # A = np.array([
    #     [0, 2, 1, 4, 3, 0, -5, -10],
    #     [-1, 3, 1, 0, 1, 3, -5, -6],
    #     [1, 1, 1, 0, 1, -2, -5, 8]
    # ], dtype=np.float128)
    # b = np.array([4, 6, 14])
    #
    # c = np.array([1, 3, -1, 3, 5, 2, -2, 0], dtype=np.float128)
    # D = np.eye(8)
    # D[2, 2] = 0
    # D[6, 6] = 0
    #
    # x = np.array([0, 2, 0, 0, 4, 0, 0, 1], dtype=np.float128)
    # J_b = [1, 4, 7]
    # J_ast = [1, 4, 7]
    # x_opt, _, _ = solve_square_problem(x, J_b, J_ast, D, c, A)
    # if x_opt is not None:
    #     print 'x optimal: [\n\t' + '\n\t'.join(map(str, x_opt)) + '\n]'
    #     print 'f(x_optimal) = ', np.dot(c, x_opt) + 0.5*np.dot(np.dot(x_opt, D), x_opt)
    #
    # print '\n\nProblem 4\n'
    # A = np.array([
    #     [0, 2, 1, 4, 3, 0, -5, -10],
    #     [-1, 1, 1, 0, 1, 1, -1, -1],
    #     [1, 1, 1, 0, 1, -2, -5, 8]
    # ], dtype=np.float128)
    # b = np.array([20, 1, 7])
    #
    # c = np.array([1, 3, 4, 3, 5, 6, -2, 0], dtype=np.float128)
    # D = np.array([
    #     [25, 10, 0, 3, -1, 13, 0, 1],
    #     [10, 45, 0, 0, 0, 20, 0, 0],
    #     [0, 0, 20, 0, 0, 0, 0, 0],
    #     [3, 0, 0, 29, -3, 15, 0, 3],
    #     [-1, 0, 0, -3, 21, -5, 0, -1],
    #     [13, 20, 0, 15, -5, 61, 0, 5],
    #     [0, 0, 0, 0, 0, 0, 20, 0],
    #     [1, 0, 0, 3, -1, 5, 0, 21]
    # ], np.float128)
    #
    # x = np.array([3, 0, 0, 2, 4, 0, 0, 0], dtype=np.float128)
    # J_b = [0, 3, 4]
    # J_ast = [0, 3, 4]
    # x_opt, _, _ = solve_square_problem(x, J_b, J_ast, D, c, A)
    # if x_opt is not None:
    #     print 'x optimal: [\n\t' + '\n\t'.join(map(str, x_opt)) + '\n]'
    #     print 'f(x_optimal) = ', np.dot(c, x_opt) + 0.5*np.dot(np.dot(x_opt, D), x_opt)
    #
    # print '\n\nProblem 5\n'
    # A = np.array([
    # [0, 0, 1, 5, 2, 0, -5, -4],
    # [1, 1, -1, 0, 1, -1, -1, -1],
    # [1, 1, 1, 0, 1, 2, 5, 8],
    # ], np.float128)
    #
    # c = np.array([1, -3, 4, 3, 5, 6, -2, 0], np.float128)
    #
    # D = np.zeros((8, 8), np.float128)
    # x = np.array([4, 0, 5, 2, 0, 0, 0, 0], np.float128)
    # J_b = [0, 2, 3]
    # J_ast = [0, 2, 3]
    # x_opt, _, _ = solve_square_problem(x, J_b, J_ast, D, c, A)
    # if x_opt is not None:
    #     print 'x optimal: [\n\t' + '\n\t'.join(map(str, x_opt)) + '\n]'
    #     print 'f(x_optimal) = ', np.dot(c, x_opt) + 0.5*np.dot(np.dot(x_opt, D), x_opt)

