from random import random

import numpy as np
from scipy.optimize import linprog


MAGIC_CONSTANT = 199
MAGIC_MULTIPLIER = 10

TESTS = (
    'test1.in',
    'test2.in',
)


def get_data(file_name):
    # with open('test.in', 'r') as f:
    #     n = int(f.readline())
    #     c = map(float, f.readline().split())
    #     d = [map(float, f.readline().split()) for _ in xrange(len(c))]
    #     m = int(f.readline())
    #     c_g = []
    #     d_g = []
    #     alpha = []
    #     for i in xrange(m):
    #         c_g.append(map(float, f.readline().split()))
    #         d_g.append([map(float, f.readline().split()) for _ in xrange(n)])
    #         alpha.append(float(f.readline()))
    #     x_line = map(float, f.readline().split())
    #     x_wave = map(float, f.readline().split())
    # d = np.matrix(d)
    # d_g = [np.matrix(x) for x in d_g]

    with open(file_name, 'r') as f:
        n, m = map(int, f.readline().split())
        b_big = np.matrix([map(float, f.readline().split()) for _ in xrange(m)])
        d = b_big.T * b_big
        c = map(float, f.readline().split())

        limits_count = int(f.readline())
        c_g = []
        d_g = []
        alpha = []

        for i in xrange(limits_count):
            b_big = np.matrix([map(float, f.readline().split()) for _ in xrange(m)])
            d_g.append(b_big.T * b_big)
            c_g.append(map(float, f.readline().split()))
            alpha.append(float(f.readline()))

        # any permitted plan
        x_permitted = map(float, f.readline().split())
        x_checking = map(float, f.readline().split())

    c = np.matrix(c).T
    c_g = [np.matrix(x).T for x in c_g]
    x_permitted = np.matrix(x_permitted).T
    x_checking = np.matrix(x_checking).T

    return n, limits_count, b_big, d, c, c_g, d_g, alpha, x_permitted, x_checking


def process(input_file):
    result = []

    n, m, b_big, d, c, c_g, d_g, alpha, x_permitted, x_checking = get_data(input_file)

    df_dx = c + d * x_checking
    dg_dx = [cc + dd * x_checking for cc, dd in zip(c_g, d_g)]

    # finding active derivatives
    active = []
    nonactive = []

    for i in xrange(m):
        if abs((c_g[i].T * x_checking + x_checking.T * d_g[i] * x_checking / 2 + alpha[i])[0, 0]) == 0.0:
            active.append(i)
        else:
            nonactive.append(i)

    # make main limitation for active derivatives
    c_fun = [df_dx[i, 0] for i in xrange(n)]
    a = [[dg_dx[index][i, 0] for i in xrange(n)] for index in active]
    b = [0 for _ in active]

    # find l asterisk  (d. < l* < d*)
    # d low (d.)
    limitation_low = [0 if x_checking[i, 0] == 0 else -1 for i in xrange(n)]
    # d high (d*)
    limitation_high = [1 for _ in xrange(n)]
    l_ast = np.matrix(linprog(c=c_fun, A_ub=a, b_ub=b, bounds=zip(limitation_low, limitation_high)).x)

    # check possibilities of optimizing
    a = (df_dx.T * l_ast.T)[0, 0]
    if a == 0.0:
        return 'optimal',

    # build x with roof
    b = ((x_permitted - x_checking).T * df_dx)[0, 0]

    # result.append(a)
    if b > 0:
        coefs = [-a / (2 * b)]
        coefs.extend(- a / ((random() * MAGIC_MULTIPLIER + 1 + 1e-9) * b) for _ in xrange(MAGIC_CONSTANT))
    else:
        coefs = [1]
        coefs.extend(random() * MAGIC_MULTIPLIER + 1e-9 for _ in xrange(MAGIC_CONSTANT))

    # condition
    ts = [0.5]
    ts.extend([random() for _ in xrange(MAGIC_CONSTANT)])

    # check condition on x with roof
    f_start = c.T * x_checking + x_checking.T * d * x_checking / 2
    f_new = None

    for t in ts:
        for coef in coefs:
            x_new = x_checking + (t * l_ast).T + coef * t * (x_permitted - x_checking)

            if any(x_new[i, 0] < 0 for i in xrange(n)):
                continue

            f_new = c.T * x_new + x_new.T * d * x_new / 2

            if f_new < f_start:
                if all(
                        (c_g[i].T * x_new + x_new.T * d_g[i] * x_new / 2 + alpha[i])[0, 0] <= 0
                        for i in xrange(m)
                ):
                    result.extend((
                        'success',
                        f_start[0, 0],
                        f_new[0, 0],
                        x_new,
                    ))
                    return result

    result.extend((f_start, f_new))
    return result


if __name__ == '__main__':
    for test in TESTS:
        print '==================================================================================================='
        print test
        print '---------------------------------------------------------------------------------------------------'
        result = process(test)
        for item in result:
            print item
        print '===================================================================================================\n\n'
